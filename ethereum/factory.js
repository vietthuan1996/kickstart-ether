import web3 from "./web3";
import CampaignFactory from './build/CampaignFactory.json';

const factoryInstance = new web3.eth.Contract(
    JSON.parse(CampaignFactory.interface),
    '0x0cfbE152dd59715FDfF9091473fE57C50407d92A'
);

export default factoryInstance;
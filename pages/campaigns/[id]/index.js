import React from "react";
import Layout from "../../../components/Layout";
import ContributeForm from "../../../components/ContributeForm";
import { Card, Grid, Button } from "semantic-ui-react";
import Link from "next/link";
import connectCampaignInstance from "../../../ethereum/campaign";
import { useRouter } from "next/router";
import web3 from "../../../ethereum/web3";

const Campaign = (props) => {
    const router = useRouter();
    const renderCampaignInfo = () => {
        const items = props.campaignInfo.map(info => {
            return {
                header: info.value,
                description: info.description,
                meta: info.meta,
                style: info.style
            }
        });

        return <Card.Group items={items} />
    };

    renderCampaignInfo();

    return (
        <Layout>
            <h3>Show Campaign</h3>
            <Grid>
                <Grid.Row>
                    <Grid.Column width={9}>
                        {renderCampaignInfo()}
                    </Grid.Column>
                    <Grid.Column width={3}>
                        <ContributeForm address={router.query.id} />
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column>
                        <Link href="/campaigns/[id]/requests" as={`/campaigns/${router.query.id}/requests`}>
                            <a href="#">
                                <Button primary>
                                    View Requests
                                </Button>
                            </a>
                        </Link>
                    </Grid.Column>

                </Grid.Row>
            </Grid>
        </Layout>
    );
};

Campaign.getInitialProps = async (ctx) => {
    const address = ctx.query.id;
    const campaign = connectCampaignInstance(address);
    const summary = await campaign.methods.getSummary().call();
    return {
        campaignInfo: [
            {
                value: summary[0],
                meta: 'Minimum Contribution',
                description: 'The minimum amount of money to contribute'
            },
            {
                value: web3.utils.fromWei(summary[1], 'ether'),
                meta: 'Balance(ether)',
                description: 'Amount of money in the account'
            },
            {
                value: summary[2],
                meta: 'Number of Requests',
                description: 'Requests try to withdraw money from the contract'
            },
            {
                value: summary[3],
                meta: 'Number of Approvers',
                description: 'Number of people who donated to the campaign'
            },
            {
                value: summary[4],
                meta: 'Manager Address',
                description: 'Manager who created this campaign',
                style: {
                    overflowWrap: 'break-word'
                }
            }
        ]
    };
};

export default Campaign;
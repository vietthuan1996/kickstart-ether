import React, { useState, useEffect } from "react";
import Layout from "../../../components/Layout";
import { Form, Button, Message, Input } from "semantic-ui-react";
import web3 from "../../../ethereum/web3";
import connectCampaignInstance from "../../../ethereum/campaign";
import Link from "next/link";
import { useRouter } from "next/router";

const NewRequest = (props) => {
    const router = useRouter();
    const [value, setValue] = useState('');
    const [description, setDescription] = useState('');
    const [recipient, setRecipient] = useState('');
    const [errorMessage, setErrorMessage] = useState('');
    const [loading, setLoading] = useState(false);

    const submitHandler = async () => {
        updateStateBeforeSubmit();
        const address = router.query.id;

        try {
            const campaign = connectCampaignInstance(address);
            const accounts = await web3.eth.getAccounts();
            await campaign.methods.createRequest(
                description,
                web3.utils.toWei(value, 'ether'),
                recipient
            ).send({
                from: accounts[0],
                gas: '1000000'
            });    
        } catch (error) {
            setErrorMessage(error.message);
        }
        setLoading(false)
    };
    const updateStateBeforeSubmit = () => {
        setLoading(true);
        setErrorMessage('');
    }

    return (
        <Layout>
            <h3>Create a Request</h3>
            <Form onSubmit={submitHandler} error={!!errorMessage}>
                <Form.Field>
                    <label htmlFor="">Description</label>
                    <Input onChange={(event) => {
                        setDescription(event.target.value);
                    }} />
                </Form.Field>
                <Form.Field>
                    <label htmlFor="">Value in Ether</label>
                    <Input onChange={(event) => {
                        setValue(event.target.value);
                    }} />
                </Form.Field>
                <Form.Field>
                    <label htmlFor="">Recipient</label>
                    <Input onChange={(event) => {
                        setRecipient(event.target.value);
                    }} />
                </Form.Field>
                <Message error negative header="Oops!" content={errorMessage} />
                <Button primary loading={loading}>
                    Create!
                </Button>
            </Form>
        </Layout>
    );
};

// NewRequest.getInitialProps = async (ctx) => {
//     const address = ctx.query.id;

// };

export default NewRequest;
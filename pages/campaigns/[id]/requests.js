import Link from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';
import { Button, Table } from 'semantic-ui-react';
import Layout from '../../../components/Layout';
import RequestRow from '../../../components/RequestRow';
import connectCampaignInstance from '../../../ethereum/campaign';
import web3 from '../../../ethereum/web3';

const Requests = (props) => {
    const router = useRouter();

    const renderRow = () => {
        return props.requests.map((request, index) => {
            return (
                <RequestRow
                    key={index}
                    numberOfDonator={props.numberOfDonator}
                    request={request}
                    index={index} 
                    address={router.query.id}
                    approveRequest={approveRequest}
                    finalizeRequest={finalizeRequest}/>
            );
        });
    };

    const approveRequest = async (index) => {
        const campaign = connectCampaignInstance(router.query.id);
        const accounts = await web3.eth.getAccounts();

        await campaign.methods.approveRequest(index).send({
            from: accounts[0],
            gas: '1000000'
        });
    };

    const finalizeRequest = async (index) => {
        const campaign = connectCampaignInstance(router.query.id);
        const accounts = await web3.eth.getAccounts();

        await campaign.methods.finalizeRequest(index).send({
            from: accounts[0],
            gas: '1000000'
        });
    }
    return (
        <Layout>
            <h3>Requests</h3>
            <Table>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>ID</Table.HeaderCell>
                        <Table.HeaderCell>Description</Table.HeaderCell>
                        <Table.HeaderCell>Amount</Table.HeaderCell>
                        <Table.HeaderCell>Recipient</Table.HeaderCell>
                        <Table.HeaderCell>Approval Count</Table.HeaderCell>
                        <Table.HeaderCell>Approve</Table.HeaderCell>
                        <Table.HeaderCell>Finalize</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {renderRow()}
                </Table.Body>
            </Table>
            <Link href={'/campaigns/[id]/newRequest'} as={`/campaigns/${router.query.id}/newRequest`}>
                <Button primary>Add Request</Button>
            </Link>
        </Layout>
    );
}

Requests.getInitialProps = async (ctx) => {
    const address = ctx.query.id;
    const campaign = connectCampaignInstance(address);
    const numberOfDonator = await campaign.methods.approversCount().call();
    const requests = [];

    const numberOfRequest = await campaign.methods.getRequestsCount().call();
    for (let index = 0; index < numberOfRequest; index++) {
        requests.push(await campaign.methods.requests(index).call());
    }
    return { requests, numberOfDonator };
};

export default Requests;
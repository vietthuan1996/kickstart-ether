import { useRouter } from "next/router";
import React, { useState } from "react";
import { Form, Button, Input, Message } from "semantic-ui-react";
import Layout from "../../components/Layout";

import factoryInstance from "../../ethereum/factory";
import web3 from "../../ethereum/web3";

const newCampaign = (props) => {
    const router = useRouter();

    const [minimumContribution, setMinimumContribution] = useState('');
    const [errorMessage, setErrorMessage] = useState('');
    const [loading, setLoading] = useState(false);

    const submitHandler = async (event) => {
        event.preventDefault();

        try {
            updateStateBeforeSubmit();
            const accounts = await web3.eth.getAccounts();
            await factoryInstance.methods
                .createCampaign(minimumContribution)
                .send({
                    from: accounts[0]
                });
            router.push('/')
        } catch (error) {
            setErrorMessage(error.message);
        }
        setLoading(false);
    };

    const updateStateBeforeSubmit = () => {
        setLoading(true);
        setErrorMessage('');
    }

    return (
        <Layout>
            <h3>New Campaign</h3>

            <Form onSubmit={submitHandler} error={!!errorMessage}>
                <Form.Field>
                    <label htmlFor="">Minimum Contribution</label>
                    <Input
                        label='wei'
                        placeholder='Put your money here!'
                        labelPosition="right"
                        value={minimumContribution}
                        onChange={event => {
                            setMinimumContribution(event.target.value);
                        }}
                    />
                </Form.Field>
                <Message error negative header="Oops!" content={errorMessage} />
                <Button loading={loading} primary>Create!</Button>
            </Form>
        </Layout>

    );
};

export default newCampaign;
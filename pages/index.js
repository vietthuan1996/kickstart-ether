import React from "react";
import { Card, Button } from "semantic-ui-react";
import Link from "next/link";
import factoryInstance from "../ethereum/factory";
import Layout from "../components/Layout";

const campaignIndex = (props) => {

    const renderCampaigns = () => {
        return props.campaigns.map(campaign => {
            return {
                header: campaign,
                description: (
                    <Link href="/campaigns/[id]" as={`/campaigns/${campaign}`}>
                        <a href="#">View Campaign</a>
                    </Link>
                ),
                fluid: true
            };
        });
    };

    return (
        <Layout>
            <h2>Open Campaign</h2>
            <Link href="/campaigns/new">
                <Button
                    floated="right"
                    content='Create Campaign'
                    icon='add circle'
                    primary />
            </Link>
            <Card.Group items={renderCampaigns()} />
        </Layout>
    );
}

campaignIndex.getInitialProps = async () => {
    const campaigns = await factoryInstance.methods.getDeployedCampaigns().call();
    return { campaigns };
};

export default campaignIndex;
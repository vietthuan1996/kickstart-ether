import React from 'react';

const Intro = (props) => {
    const { info } = props;

    return (
        <div>{info}</div>
    );
};

export default Intro;
import React from 'react';
import { Table, Button } from 'semantic-ui-react';
import web3 from '../ethereum/web3';

const RequestRow = (props) => {
    const { request, index, address, numberOfDonator, approveRequest, finalizeRequest } = props;
    const isReady = request.approvalCount > numberOfDonator / 2;
    return (
        <Table.Row key={index} disabled={request.complete} positive={isReady && !request.complete}>
            <Table.Cell>{index}</Table.Cell>
            <Table.Cell>{request.description}</Table.Cell>
            <Table.Cell>{web3.utils.fromWei(request.value, 'ether')}</Table.Cell>
            <Table.Cell>{request.recipient}</Table.Cell>
            <Table.Cell>{`${request.approvalCount}/${numberOfDonator}`}</Table.Cell>
            <Table.Cell>
                {
                    !request.complete && <Button color='red' basic onClick={() => {
                        approveRequest(index);
                    }}>
                        Approve
                    </Button>
                }
            </Table.Cell>
            <Table.Cell>
                {
                    !request.complete && <Button color='green' basic onClick={() => {
                        finalizeRequest(index);
                    }}>Finalize
                    </Button>
                }
            </Table.Cell>
        </Table.Row>
    );
};

export default RequestRow;
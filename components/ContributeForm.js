import { useRouter } from "next/router";
import React, { useState } from "react";
import { Form, Input, Button, Message } from "semantic-ui-react";
import connectCampaignInstance from "../ethereum/campaign";
import web3 from "../ethereum/web3";

const ContributeForm = (props) => {
    const router = useRouter();

    const { address } = props;
    const [value, setValue] = useState('');
    const [errorMessage, setErrorMessage] = useState('');
    const [loading, setLoading] = useState(false);

    const submitHandler = async (event) => {
        event.preventDefault();

        updateStatesBeforeSubmit();
        const campaign = connectCampaignInstance(address);

        try {
            const accounts = await web3.eth.getAccounts();
            await campaign.methods.contribute().send({
                from: accounts[0],
                value: web3.utils.toWei(value, 'ether')
            });
            router.reload();
        } catch (error) {
            setErrorMessage(error.message);
        }
        setLoading(false);
    };

    const updateStatesBeforeSubmit = () => {
        setLoading(true);
        setErrorMessage('');
    }

    return (
        <Form onSubmit={submitHandler} error={!!errorMessage}>
            <Form.Field>
                <label htmlFor="">Amount to Contribute</label>
                <Input
                    label='ether'
                    placeholder='Put your money here!'
                    labelPosition="right"
                    value={value}
                    onChange={(event) => {
                        setValue(event.target.value);
                    }}
                />
            </Form.Field>
            <Message error negative header="Oops!" content={errorMessage} />
            <Button loading={loading} primary>Contribute!</Button>
        </Form>
    );
};

export default ContributeForm;